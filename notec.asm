; --- Justyna Micota jm418427

ZERO                    equ     48              ;ascii code for '0'
NINE                    equ     57              ;ascii code for '9'
BIG_DIFF                equ     55              ;difference between 10 and ascii code for 'A'
SMALL_DIFF              equ     87              ;difference between 10 and ascii code for 'a'
BIG_F                   equ     70              ;ascii code for 'F'
SMALL_F                 equ     102             ;ascii code for 'f'
LAST_FOUR_BITS          equ     15              ;mask for checking modulo 16

;R8, R9             - processing registers for storing temporary data
;R10                - input mode flag
;R12                - saves stack pointer
;R13                - saves current hexadecimal value from input
;R14                - ptr to position in string from function argument
;R15                - saves notec nr (incremented)

; Check if char is a digit for one range of acceptable characters ([0,9], [a,f], [A,F])
; and convert to value from hexadecimal code by subtracting value given in 1st argument.
%macro check_digit_in_range 2
         mov        r8, rax
         cmp        r8, %2                      ;cmp to greatest ascii code acceptable
         ja         is_not_digit                ;skip to processing non-digit character
         
         sub        rax, %1                     ;convert to value
         jmp        is_digit
%endmacro

; Set input mode flag and check its previous state, jump to label from 2nd argument
%macro update_input_mode 2
         mov        r9, r10
         mov        r10, %1                     ;change input mode flag
         cmp        r9, %1                      ;check if was in / out of input mode
         je         %2                          ;jump to given label
%endmacro

; Get one character from function arguments string, quit if end of string
%macro get_char_to_rax 0
get_one_char:
         cmp        byte [r14], 0x00            ;check if input finished
         je         finish
         xor        rax, rax                    ;clear rax
         mov        al, byte [r14]              ;get one character
         inc        r14                         ;move to next position in string
         jmp        start_processing            ;jump to processing the character
finish:
         cmp        r10, 1                      ;check if last char was value to push
         jne        quit                        ;   (check input mode flag)
         push       r13                         ;if it was, push it to stack
quit:
         pop        rax
         mov        rsp, r12                    ;restore old stack ptr
         restore_abi_registers
         ret
%endmacro

; Mutex open = 0, closed = 1
%macro get_mutex 1
%%busy_wait:
         xor        eax, eax
         mov        eax, 1
         xchg       dword [%1], eax             ; (atomic)
         test       eax, eax                    ;check if mutex open (0)
         jnz        %%busy_wait                 ;else: work inside critical section
%endmacro
         
%macro free_mutex 1
         xor        eax, eax
         xchg       dword [%1], eax             ;open mutex (put 0) (atomic)
%endmacro

; Wait until my position in privateWait array is 1 (received a signal to stop waiting)
%macro wait_for_signal_r15 0
         mov        rcx, private_wait           ;get effective memory address
%%busy_wait_signal:
         xor        eax, eax
         xchg       dword [rcx + r15 * 4], eax  ; (atomic)
         test       eax, eax                    ;check if signal received (1)
         jz         %%busy_wait_signal 
%endmacro

; Signal position from r9 in privateWait array
%macro signal_r9 0
         mov        eax, 1                      ;signal value
         mov        rcx, private_wait
         xchg       dword [rcx + r9 * 4], eax   ;pass the signal (atomic)
%endmacro

%macro push_abi_registers 0
         push       rbx
         push       rbp
         push       r12
         push       r13 
         push       r14
         push       r15
%endmacro

%macro restore_abi_registers 0
         pop        r15 
         pop        r14
         pop        r13
         pop        r12
         pop        rbp
         pop        rbx
%endmacro

global notec
extern debug

section .bss
         declarations_mutex     resd 1      ;exclusive access to declarations array
         declarations           resd N+1    ;who am i waiting for on display for others
         private_wait           resd N+1    ;for private use by thread with this number
         pipe_mutex             resd 1      ;exclusive access to pipe
         pipe                   resq 1      ;values exchanged through pipe by one pair
                                            ;   of threads at a time
section .text

notec:
         push_abi_registers
         mov        r15, rdi                ;save notec nr
         inc        r15                     ;increment for correct communication
                                            ;  (.bss section initiated with 0, which is
                                            ;  valid notec nr -> invalidating it)
         mov        r14, rsi                ;save ptr to string
         xor        r13, r13                ;reset input mode value counter
         xor        r10, r10                ;mark as not in input mode
         mov        r12, rsp
get_byte:
         get_char_to_rax                    ;(changes rax, r14, r10, handles end of str)
start_processing:
         mov        r8, rax
         cmp        r8, '0'                 ;compare to lowest ascii code acceptable
         jb         is_not_digit            ;if less, isnt a digit

         mov        r8, rax
         cmp        r8, 'A'                 ;compare to next acceptable range
         jae        skip_0_to_9             
         check_digit_in_range ZERO, NINE    ;else: in [0,9] to be a digit

skip_0_to_9:     
         mov        r8, rax
         cmp        r8, 'a'                 ;cmp to beginning of last acceptable range
         jae        skip_A_to_F
         check_digit_in_range BIG_DIFF, BIG_F       ;else: in [A,F] to be a digit

skip_A_to_F:                                
         check_digit_in_range SMALL_DIFF, SMALL_F   ;in [a,f] to be a digit

is_digit:                                  
         update_input_mode 1, in_input_mode ;update input mode flag to 1

in_input_mode:
         shl        r13, 4                  ;multiply current value by 16
         add        r13, rax                ;add new digit to result
         jmp        get_byte

is_not_digit:                               ;update input mode flag to 0
         update_input_mode 0, dont_push     ;if previously not input mode: jmp dont_push
                                            ;else:
         push       r13                     ;push input value to stack
         xor        r13, r13                ;restart hexa value counter
dont_push:
         cmp        al, '='
         je         get_byte                ;input mode already updated, get next char

         cmp        al, '+'                 
         jne        multiply                ;if operation not '+', check next option
         pop        rax                     ;get arguments for operation from stack
         pop        r8                      
         add        rax, r8                 ;perform operation
         push       rax                     ;push result
         jmp        get_byte                ;get next character
multiply:
         cmp        al, '*'                 ; -- analogically...
         jne        minus
         pop        rax
         pop        r8
         mul        r8
         push       rax 
         jmp        get_byte
minus:                                      
         cmp        al, '-'
         jne        do_and
         pop        rax
         neg        rax
         push       rax
         jmp        get_byte
do_and:
         cmp        al, '&'
         jne        do_or
         pop        rax
         pop        r8
         and        rax, r8
         push       rax 
         jmp        get_byte
do_or:
         cmp        al, '|'
         jne        do_Xor
         pop        rax
         pop        r8
         or         rax, r8
         push       rax 
         jmp        get_byte
do_Xor:
         cmp        al, '^'
         jne        negate
         pop        rax
         pop        r8
         xor        rax, r8
         push       rax 
         jmp        get_byte
negate:                 
         cmp        al, '~'
         jne        do_Z
         pop        rax
         not        rax
         push       rax 
         jmp        get_byte
do_Z:                                       ;remove stack top
         cmp        rax, 'Z'
         jne        do_Y
         pop        rax
         jmp        get_byte
do_Y:                                       ;duplicate stack top
         cmp        rax, 'Y'
         jne        do_X
         pop        rax 
         push       rax
         push       rax
         jmp        get_byte
do_X:                                       ;swap two values on stack top
         cmp        al, 'X'
         jne        push_N
         pop        rax
         pop        r8
         push       rax 
         push       r8
         jmp        get_byte
push_N:                                     ;push number of threads on stack
         cmp        al, 'N'
         jne        push_my_nr
         mov        rax, N 
         push       rax 
         jmp        get_byte
push_my_nr:                                 ;push number of this noteć on stack
         cmp        al, 'n'
         jne        do_debug
         mov        rax, r15
         dec        rax
         push       rax
         jmp        get_byte
do_debug:                                   ;call debug function
         cmp        al, 'g'
         jne        do_W
                                            ;pass arguments to debug:
         mov        rdi, r15                ;-pass number of this noteć
         dec        rdi                     ;decrement for rules of outside the program
         mov        rsi, rsp                ;-pass stack pointer

         mov        rax, rsp
         and        rax, LAST_FOUR_BITS     ;get last 4 bits from rsp value
         test       rax, rax                ;4 last bits should be 0 (aligned to 16)
         jz         dont_align_stack        ;if aligned jump to different calling mode
         
         push       0                       ;align (push 8 bits)
         call       debug
         pop        rax                     ;reverse alignment
         jmp        after_debug

dont_align_stack:   
         call       debug

after_debug:
         mov        r8, 8
         mul        r8                      ;multiply debug result
         add        rsp, rax                ;move stack
         jmp        get_byte                ;jump to processing next character
        
do_W:                                           ; - r15 = this notec nr      
         pop        r9                          ; - r9 = nr this notec waits for
         inc        r9                          ;adjust partner nr to code rules
         
         get_mutex  rel declarations_mutex      ; [exclusive access to declarations]
         mov        rcx, declarations           ;get address of declarations array
         cmp        dword [rcx + r9 * 4], r15d  ;check if partner is waiting for me
         je         is_waiting_for_me           ;else - i have to wait for partner:

         mov        rcx, declarations
         mov        dword [rcx + r15 * 4], r9d  ;declare waiting for partner
         
         free_mutex rel declarations_mutex      ; [let go of access to declarations]

         wait_for_signal_r15                    ; (uses rax, rcx, r8)
                                                ; [INHERITED pipe mutex]
         xor        r8, r8
         mov        r8, qword [rel pipe]        ;get partners value (already in pipe)
         pop        rax                         ;get my stack top
         mov        qword [rel pipe], rax       ;pass my stack top to pipe
         push       r8                          ;*switch* push partners value on stack

         signal_r9                              ;signal partner that pipe is ready
                                                ; [passing exclusive access to pipe]
         jmp        get_byte                    ;communication over for this notec

is_waiting_for_me:                              ;partner thread is waiting for me
         mov        rcx, declarations           ;get address of declarations array
         xor        r8, r8
         mov        dword [rcx + r9 * 4], r8d   ;clear partners declaration (declare 0)

         free_mutex rel declarations_mutex      ; [let go of access to declarations]

         get_mutex  rel pipe_mutex              ;wait for [exclusive access to pipe]
         xor        r8, r8
         pop        r8                          ;get my stack top
         mov        qword [rel pipe], r8        ;pass my stack top to pipe

         signal_r9                              ;signal partner that pipe is ready
                                                ; [partner INHERITS pipe mutex]
         wait_for_signal_r15                    ;wait when partner puts his value in pipe
                                                ; [INHERITED pipe mutex back]
         mov        r8, qword [rel pipe]        ;get partners stack top from pipe
         push       r8                          ;*switch* push partners value on stack

         free_mutex rel pipe_mutex              ; [let go of exclusive access to pipe]

         jmp        get_byte                    ;communication over for this notec
